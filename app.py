# Import classs
from GithubReleaseNotifier import GithubReleaseNotifier

# Set options
options = {
	# Set repositories here in an array
    'repositories': ['PACCommunity/PAC', 'SmartCash/Core-Smart'],

    # Set runType
    # Option 'cron': script runs once
    # Option 'interval': script runs every x seconds
    # default: 'cron'
    'runType': 'cron',

    # If runType is set to 'interval', please set the interval to run
   	# default: 60*60*24 sec = 1 day
    'interval': 60*60*24,

    # Set notification email
    'email': 'inigovandijk@gmail.com'
}

# Go
notifier = GithubReleaseNotifier(options)