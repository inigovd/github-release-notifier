import os
import json
import time
import smtplib
import requests
from dotenv import load_dotenv
from email.mime.text import MIMEText
from requests.auth import HTTPBasicAuth
from email.mime.multipart import MIMEMultipart

load_dotenv()

class GithubReleaseNotifier:
	def __init__(self, options):
		# Constructor for this class
		self.versions = {}

		# Set repositories
		if 'repositories' in options.keys():
			self.setRepos(options['repositories'])

		# Set interval
		self.interval = 60*60*24 # Once a day
		if 'interval' in options.keys():
			self.interval = options['interval']

		# Set runType
		self.runType = 'cron'
		if 'runType' in options.keys():
			self.runType = options['runType']

		# Set email
		self.email = ''
		if 'email' in options.keys():
			self.email = options['email']

		self.sync()

	# Get the latest release versions for the stored repositories
	def sync(self):
		if len(self.versions) == 0:
			print("No repositories set.")
			return

		for repo in self.versions:
			r = requests.get('https://api.github.com/repos/' + repo + '/releases/latest')
			json = r.json()
			version = json['tag_name']
			self.check(repo, version)
			self.versions[repo] = version

		self.save()

		# Keep going if runType == 'interval'
		if self.runType == 'interval':
			time.sleep(self.interval)
			self.sync()
		else:
			print("Finished")

	# Set the repositories
	def setRepos(self, repos):
		for repo in repos:
			self.versions[repo] = ''

	# Add a repository
	def addRepo(self, repo):
		self.versions[repo] = ''
		print("Added repo: " + repo)
		self.sync()

	# Load repositories from versions.json file
	def load(self):
		with open('versions.json') as infile:
			try:
				return json.load(infile)
			except ValueError:
				return {}

	# Write repositories to versions.json
	def save(self):
		with open('versions.json', 'w') as outfile:
			json.dump(self.versions, outfile)

	# Check if version has changed
	def check(self, repo, version):
		old = self.load()
		if repo in old.keys() and old[repo] != version:
			print(repo + " has a new version: " + version)
			self.notify(repo, version)
		else:
			print(repo + " latest version: "  + version)

	# Notify per email
	def notify(self, repo, version):
		domain = os.getenv('MAILGUN_DOMAIN')
		if len(domain) == 0:
			print("Please set your MAILGUN_API_KEY in your .env file")
			return

		url = 'https://api.mailgun.net/v3/' + domain + '/messages'

		api_key = os.getenv('MAILGUN_API_KEY')
		if len(api_key) == 0:
			print("Please set your MAILGUN_API_KEY in your .env file")
			return

		from_address = os.getenv('MAILGUN_FROM_ADDRESS')
		if len(from_address) == 0:
			print("Please set your MAILGUN_FROM_ADDRESS in your .env file")
			return

		data = {
			'from': from_address,
			'to': self.email,
			'subject': repo + ' has a new version: ' + version,
			'text': 'Repository ' + repo + ' has a new version: ' + version
		}

		requests.post(url, auth = HTTPBasicAuth('api', api_key), data = data)

		print('  Notify: ' + 'Repository ' + repo + ' has a new version: ' + version)
		print('  Notify: sent to ' + self.email)