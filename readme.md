# Welcome
Welcome to this simple tool to help you stay updated with repository releases on GitHub.

# Dependencies
- python-dotenv

## How to use
1. Clone this repo (git clone https://bitbucket.org/inigovd/github-release-notifier)
2. Install dependencies (pip install python-dotenv)
3. Set options in app.py
4. Run app.py yourself or by cron (python app.py)
5. Create a .env file and add these lines and add your mailgun API key and from address, e.g.:
`MAILGUN_API_KEY=YOUR_MAILGUN_API_KEY_HERE`
`MAILGUN_FROM_ADDRESS=YOUR@FROM.ADDRESS`
`MAILGUN_DOMAIN=YOUR.MAILGUN.DOMAIN`

The repository versions are automatically saved into versions.json. If a new version is detected, an email is sent to the email you set in the options.

To add a cronjob to execute this script, e.g. for once a day, do crontab -e and add this line:
```
0 0 * * * /usr/bin/python /path/to/github-release-notifier/app.py
```

## Options
`repositories`  
The repositories to watch, in 'owner:repo' format. Has to be an array.  
e.g. ['SmartCash/Core-Smart', 'PACCommunity/PAC']

`runType`  
Either 'cron' or 'interval'.  
When set to 'cron', the script runs once, you should create a crontab entry to make sure it runs at a certain interval.  
When set to 'interval', the script will wait that amount of seconds before syncing again.

`email`  
The email address to nofity to. Leave empty if you don't want to use email notifications.